import random

from gavo.grammars.customgrammar import CustomRowIterator

# create data/laststamp and fill it with 0 to start

class RowIterator(CustomRowIterator):
  def _iterRows(self):
    with open(self.sourceToken) as f:
      lastid = int(f.read())

    # custom code here
    for offset in range(10):
	    yield {"id": lastid, "value": random.random()}
	    lastid += 1

    with open(self.sourceToken, "w") as f:
      f.write("%d\n"%(lastid+1))


if __name__=="__main__":
  import sys

  from gavo.grammars.customgrammar import CustomGrammar
  ri = RowIterator(CustomGrammar(None), "data/debugstate")
  for row in ri:
    print(row)
