<resource schema="svc_2019">
  <meta name="creationDate">2020-01-09T10:47:35Z</meta>

  <meta name="title">APERTIF Data 2019</meta>
  <meta name="description">
    %this should be a paragraph or two (take care to mention salient terms)%
  </meta>
  <!-- Take keywords from 
    http://astrothesaurus.org/thesaurus
    if at all possible -->
  <meta name="subject">%keywords; repeat the element as needed%</meta>

  <meta name="creator">%authors in the format Last, F.I; Next, A.%</meta>
  <meta name="instrument">APERTIF Correlator at Westerbork</meta>
  <meta name="facility">Westerbork Radio Observatory</meta>

  <meta name="source">%ideally, a bibcode%</meta>
  <meta name="contentLevel">Research</meta>
  <meta name="type">Catalog</meta>  <!-- or Archive, Survey, Simulation -->

  <meta name="coverage.waveband">Radio</meta>

  <STREAM id="localcolumns">
    <column name="beam_major"
      unit="arcsec" ucd="phys.angsize;instr.beam"
      tablehead="Beam (maj)"
      description="Major axis of beam (perhaps half-sensitivity) size."
      verbLevel="15"/>
    <column name="beam_minor"
      unit="arcsec" ucd="phys.angsize;instr.beam"
      tablehead="Beam (min)"
      description="Minor axis of beam (perhaps half-sensitivity) size."
      verbLevel="15"/>
    <column name="beam_pa"
      unit="degree" ucd="pos.angsize;instr.beam"
      tablehead="Beam (PA)"
      description="Position agle of beam."
      verbLevel="15"/>
    <column name="object" type="text"
      ucd="meta.id;src"
      tablehead="Object"
      description="Target object observed"
      verbLevel="10"/>
    <column name="obsid" type="text"
      ucd="meta.id;obs"
      tablehead="Obs. Id"
      description="APERTIF observation id; use datalink to discover other
        data products from this observation."
      verbLevel="5"/>
    <column name="datalink" type="text"
 			tablehead="Datalink Access"
			description="URL of a datalink document for the dataset (raw data,
			  derived data"
			displayHint="type=url">
			<property name="targetType"
				>application/x-votable+xml;content=datalink</property>
			<property name="targetTitle">Datalink</property>
		</column>
  </STREAM>

  <table id="main" onDisk="True" mixin="//siap#pgs" adql="False">
    
    <meta name="_associatedDatalinkService">
      <meta name="serviceId">dl</meta>
      <meta name="idColumn">accref</meta>
    </meta>

<!--    <mixin
      calibLevel="2"
      collectionName="'%a few letters identifying this data%'"
      targetName="%column name of an object designation%"
      expTime="%column name of an exposure time%"
      targetClass="'%simbad taget class%'"
    >//obscore#publishSIAP</mixin>-->

    <FEED source="localcolumns"/>
  </table>

  <coverage>
    <updater sourceTable="main"/>
  </coverage>

  <data id="import">
    <sources pattern="data/*.pickle" recurse="True"/>

    <embeddedGrammar>
      <!-- this parses from a pickle of the server-side files generated
      by a little script called dump_fits_headers.py (that's hopefully
      on the upstream server) -->

      <iterator>
        <code>
          import pickle
          from gavo.utils import fitstools
          
          with open(self.sourceToken, "rb") as f:
            fitses = pickle.load(f)
          
          for fitsdesc in fitses:
            # relpath must be the path relative to the root URL given below.
            relpath = fitsdesc["name"][2:]

            rawdict = dict(
                (card.keyword.replace("-", "_"), card.value)
              for card in fitstools.parseCards(fitsdesc["header"]))
            rawdict["relpath"] = relpath
            rawdict["accref"] = "svc_2019/"+relpath
            rawdict["fsize"] = fitsdesc["size"]
            yield rawdict
        </code>
      </iterator>
      <rowfilter procDef="//products#define">
        <bind key="table">"\schema.data"</bind>
        <bind key="accref">@accref</bind>
        <bind key="fsize">@fsize</bind>
        <bind key="path"
          >"https://alta.astron.nl/webdav/SVC_2019_Imaging/"+@relpath</bind>
        <bind key="preview"
          >"https://alta.astron.nl/alta-static/media/"+(
            @relpath[:-5]+".png")</bind>
        <bind key="preview_mime">"image/png"</bind>
      </rowfilter>

    </embeddedGrammar>

    <make table="main">
      <rowmaker>
        <map key="beam_major" nullExcs="KeyError">@BMAJ</map>
        <map key="beam_minor" nullExcs="KeyError">@BMIN</map>
        <map key="beam_pa" nullExcs="KeyError">@BPA</map>
        <map key="object">@OBJECT</map>
        <map key="obsid">@relpath.split("/")[0]</map>
        <map key="datalink">\dlMetaURI{dl}</map>

        <apply procDef="//siap#setMeta">
          <bind key="dateObs">@DATE_OBS</bind>
          <bind key="bandpassId">"L-Band"</bind>
          <bind key="pixflags">"F"</bind>
          <bind key="title">"APERTIF %s %s"%(@DATE_OBS, @OBJECT)</bind>
        </apply>

        <apply>
          <code>
            if "NAXIS4" in vars:
              if @NAXIS4==1:
                @NAXIS -= 1
                if @NAXIS3==1:
                  @NAXIS -= 1
          </code>
        </apply>

        <apply procDef="//siap#computePGS"/>

        <apply name="computeBandpass">
          <code>
            # right now, the spectral axis is always 3, so let's try
            # this.  If this ever becomes variable, we'll probably
            # want to look at CTYPEn for FREQ-OBS
            specAxis = getWCSAxis(vars, 3, True)
            nSpecPix = vars.get("NAXIS3")
            if not nSpecPix:
              return
            elif nSpecPix==1:
              result["bandpassRefval"] = LIGHT_C/specAxis.pixToPhys(1)
              result["bandpassLo"] = LIGHT_C/specAxis.pixToPhys(0.5)
              result["bandpassHi"] = LIGHT_C/specAxis.pixToPhys(1.5)
            elif nSpecPix>1:
              result["bandpassRefval"] = LIGHT_C/specAxis.pixToPhys(nSpecPix/2)
              result["bandpassHi"] = LIGHT_C/specAxis.pixToPhys(nSpecPix+0.5)
              result["bandpassLo"] = LIGHT_C/specAxis.pixToPhys(0.5)
          </code>
        </apply>

      </rowmaker>
    </make>
  </data>

  <service id="dl" allowed="dlmeta">
    <meta name="description">
      This datalink service gives all data products related to an observation.
      Pass in accrefs of any product that's resulted from the observation.
    </meta>

    <datalinkCore>
      <descriptorGenerator>
        <code>
          accref = pubDID.split("?")[-1]
          desc = ProductDescriptor.fromAccref(pubDID, accref)
          return desc
        </code>
      </descriptorGenerator>

      <metaMaker>
        <setup>
          <par name="fnameToMeta"> {
            "image_mf_V.fits": (
              "#derivation",
              "Continuum image generated by summing along the spectral axis"),
          }</par>
        </setup>
        <code>
          obsid = descriptor.pubDID.split("/")[1]
          with base.getTableConn() as conn:
            for row in conn.queryToDicts(
                "SELECT * FROM \schema.main"
                " WHERE obsid=%(obsid)s", locals()):
              
              sem, desc = fnameToMeta.get(
                row["accref"].split("/")[-1],
                ("#unknown", "Missing Description"))

              yield descriptor.makeLink(
                row["accref"],
                semantics=sem,
                description=desc,
                contentType="image/fits",
                contentLength=row["accsize"])
        </code>
      </metaMaker>
    </datalinkCore>
  </service>

  <!-- if you want to build an attractive form-based service from
    SIAP, you probably want to have a custom form service; for
    just basic functionality, this should do, however. -->
  <service id="i" allowed="form,siap.xml">
    <meta name="shortName">%up to 16 characters%</meta>

    <!-- other sia.types: Cutout, Mosaic, Atlas -->
    <meta name="sia.type">Pointed</meta>
    
    <meta name="testQuery.pos.ra">%ra one finds an image at%</meta>
    <meta name="testQuery.pos.dec">%dec one finds an image at%</meta>
    <meta name="testQuery.size.ra">0.1</meta>
    <meta name="testQuery.size.dec">0.1</meta>

    <!-- this is the VO publication -->
    <publish render="scs.xml" sets="ivo_managed"/>
    <!-- this puts the service on the root page -->
    <publish render="form" sets="local,ivo_managed"/>
    <!-- all publish elements only become active after you run
      dachs pub q -->

    <dbCore queriedTable="main">
      <condDesc original="//siap#protoInput"/>
      <condDesc original="//siap#humanInput"/>
      <!-- enable further parameters like
        <condDesc buildFrom="dateObs"/>

        or

        <condDesc>
          <inputKey name="object" type="text" 
              tablehead="Target Object" 
              description="Object being observed, Simbad-resolvable form"
              ucd="meta.name" verbLevel="5" required="True">
              <values fromdb="object FROM lensunion.main"/>
          </inputKey>
        </condDesc> -->
    </dbCore>
  </service>

  <regSuite title="svc_2019 regression">
    <!-- see http://docs.g-vo.org/DaCHS/ref.html#regression-testing
      for more info on these. -->

    <regTest title="svc_2019 SIAP serves some data">
      <url POS="%ra,dec that has a bit of data%" SIZE="0.1,0.1"
        >i/siap.xml</url>
      <code>
        <!-- to figure out some good strings to use here, run
          dachs test -D tmp.xml q
          and look at tmp.xml -->
        self.assertHasStrings(
          "%some characteristic string returned by the query%",
          "%another characteristic string returned by the query%")
      </code>
    </regTest>

    <!-- add more tests: image actually delivered, form-based service
      renders custom widgets, etc. -->
  </regSuite>
</resource>
