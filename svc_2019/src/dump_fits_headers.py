#!/usr/bin/python3
"""
A minimal script to create a dump of primary headers of fits files below
where this script is started.

The result is a pickle of a list of dictionaries {name, size, mtime, header}.
"""

import os
import pickle

CARD_SIZE = 80

END_CARD = b'END'+b' '*(CARD_SIZE-3)

FITS_BLOCK_SIZE = CARD_SIZE*36


def read_header_bytes(f, maxHeaderBlocks=80):
	"""returns the bytes beloning to a FITS header starting at the current
	position within the file f.

	If the header is not complete after reading maxHeaderBlocks blocks,
	an IOError is raised.
	"""
	parts = []

	while True:
		block = f.read(FITS_BLOCK_SIZE)
		if not block:
			raise IOError('Premature end of file while reading header')

		parts.append(block)
		endCardPos = block.find(END_CARD)
		if not endCardPos%CARD_SIZE:
			break

		if len(parts)>=maxHeaderBlocks:
			raise IOError("No end card found within %d blocks"%maxHeaderBlocks)
	return b"".join(parts)


def make_fits_pack(fname):
	with open(fname, "rb") as f:
		header = read_header_bytes(f)

	return {
		"name": fname,
		"size": os.path.getsize(fname),
		"mtime": os.path.getmtime(fname),
		"header": header}


def iter_fitses():
	for dirpath, dirnames, filenames in os.walk("."):
		for fname in filenames:
			if fname.endswith(".fits"):
				yield make_fits_pack(
					os.path.join(dirpath, fname))


def main():
	fitses = list(iter_fitses())
	with open("fitses.pickle", "wb") as f:
		pickle.dump(fitses, f)


if __name__=="__main__":
	main()
